package com.seb.job.task.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class DemoCurrencyChangesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoCurrencyChangesApplication.class, args);
    }

}

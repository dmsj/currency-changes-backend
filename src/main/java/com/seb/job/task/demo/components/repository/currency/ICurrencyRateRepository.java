package com.seb.job.task.demo.components.repository.currency;


import com.seb.job.task.demo.components.entities.currency.CurrencyRateEntity;
import com.seb.job.task.demo.components.entities.currency.CurrencyRateId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ICurrencyRateRepository extends CrudRepository<CurrencyRateEntity, CurrencyRateId> {

    @Transactional(readOnly = true)
    @Query("select cr from CurrencyRateEntity cr where cr.id.date = ?1")
    List<CurrencyRateEntity> findByDate(String date);

}

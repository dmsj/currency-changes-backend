package com.seb.job.task.demo.components.services.currency;


import com.seb.job.task.demo.components.entities.currency.CurrencyRateEntity;
import com.seb.job.task.demo.models.currency.CurrencyChanges;
import com.seb.job.task.demo.models.currency.CurrencyChangesResponse;

import java.util.List;

public interface ICurrencyChangesService {
    List<CurrencyChanges> getCurrencyChangesByDates(String d1, String d2);

    CurrencyChangesResponse getCurrencyChangesByDate(String date);

    List<CurrencyRateEntity> getCurrencyRateByDate(String date);

    List<CurrencyRateEntity> findByDate(String date);

    List<CurrencyChanges> getCurrencyChanges(List<CurrencyRateEntity> c1, List<CurrencyRateEntity> c2);
}

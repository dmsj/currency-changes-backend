package com.seb.job.task.demo.models.currency;


import com.seb.job.task.demo.components.entities.currency.CurrencyRateEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CurrencyChanges {

    private final String dateRange;
    private final BigDecimal ratioDiff;
    private final float oldRatio;
    private final float newRatio;
    private final String currency;

    public CurrencyChanges(CurrencyRateEntity oldRatio, CurrencyRateEntity newRatio) {
        dateRange = String.format("%s-%s", oldRatio.getDate(), newRatio.getDate());
        currency = oldRatio.getCurrency();
        this.oldRatio = oldRatio.getRate();
        this.newRatio = newRatio.getRate();
        BigDecimal temp = new BigDecimal(oldRatio.getRate()).subtract(new BigDecimal(newRatio.getRate()));
        ratioDiff = temp.setScale(4, RoundingMode.HALF_UP);
    }

    public String getDateRange() {
        return dateRange;
    }

    public BigDecimal getRatioDiff() {
        return ratioDiff;
    }

    public String getCurrency() {
        return currency;
    }

    public float getOldRatio() {
        return oldRatio;
    }

    public float getNewRatio() {
        return newRatio;
    }
}

package com.seb.job.task.demo.components.controllers.currency;

import com.seb.job.task.demo.components.services.currency.ICurrencyChangesService;
import com.seb.job.task.demo.models.currency.CurrencyChangesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/currency/changes")
public class CurrencyChangesController {

    Logger log = LoggerFactory.getLogger(CurrencyChangesController.class);
    @Autowired
    private ICurrencyChangesService currencyExchangeService;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CurrencyChangesResponse> getChanges(@RequestParam("date") String date) {
        return new ResponseEntity<>(currencyExchangeService.getCurrencyChangesByDate(date), HttpStatus.OK);
    }

}

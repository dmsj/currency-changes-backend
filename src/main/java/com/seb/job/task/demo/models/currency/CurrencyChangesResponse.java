package com.seb.job.task.demo.models.currency;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.job.task.demo.models.errors.ResponseError;

import java.util.List;

public class CurrencyChangesResponse {

    @JsonProperty("changes")
    private List<CurrencyChanges> currencyChangesList;
    private ResponseError error;

    public CurrencyChangesResponse(List<CurrencyChanges> currencyChangesList, ResponseError error) {
        this.currencyChangesList = currencyChangesList;
        this.error = error;
    }

    public List<CurrencyChanges> getCurrencyChangesList() {
        return currencyChangesList;
    }

    public void setCurrencyChangesList(List<CurrencyChanges> currencyChangesList) {
        this.currencyChangesList = currencyChangesList;
    }

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }
}

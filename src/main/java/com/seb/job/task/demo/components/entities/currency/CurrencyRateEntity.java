package com.seb.job.task.demo.components.entities.currency;


import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "currency_rates")
@XmlRootElement(name = "item")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class CurrencyRateEntity {

    @EmbeddedId
    private CurrencyRateId id;
    private int quantity;
    private float rate;
    private String unit;

    public CurrencyRateEntity() {
        id = new CurrencyRateId();
    }

    @XmlAttribute(name = "date")
    public String getDate() {
        return id.getDate();
    }

    public void setDate(String date) {
        id.setDate(date);
    }

    @XmlAttribute(name = "currency")
    public String getCurrency() {
        return id.getCurrency();
    }

    public void setCurrency(String currency) {
        id.setCurrency(currency);
    }

    @XmlAttribute(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @XmlAttribute(name = "rate")
    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @XmlAttribute(name = "unit")
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public CurrencyRateId getId() {
        return id;
    }

    public void setId(CurrencyRateId id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CurrencyRateEntity{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", rate=" + rate +
                ", unit='" + unit + '\'' +
                '}';
    }
}

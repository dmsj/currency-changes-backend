package com.seb.job.task.demo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

public class DateUtil {
    /**
     * Short date format yyyy.MM.dd
     */
    public static SimpleDateFormat SHORT_DATE = new SimpleDateFormat("yyyy.MM.dd");

    /**
     * Tries parse @date to short date format Date object.<br/>
     * <font color='red'>Supported date format yyyy.MM.dd</font>
     *
     * @param date
     * @return Optional of short date format if successfully parse @date to Date object otherwise Optional.empty
     */
    public static Optional<Date> getDateOf(String date) {
        if (date == null || date.isEmpty())
            return Optional.empty();
        Date d = null;
        try {
            d = SHORT_DATE.parse(date);
        } catch (ParseException e) {
            return Optional.empty();
        }
        return Optional.of(d);
    }

    /**
     * Tries to parse @date to Date object and increase or reduce count of days of that Date by @days value<br/>
     * Date modification are made by Calendar instance so min and max date values depends on Calendar class.<br/>
     * <font color='red'>Supported date format yyyy.MM.dd</font>
     *
     * @param date
     * @param days
     * @return Optional of Date object or Optional.empty
     */
    public static Optional<Date> getChangedDate(String date, int days) {
        Optional<Date> oDate = getDateOf(date);
        if (oDate.isPresent()) {
            return Optional.of(getChangedDate(oDate.get(), days));
        }
        return Optional.empty();
    }

    /**
     * Date modification are made by Calendar instance so min and max date values depends on Calendar class.
     *
     * @param date (not null)
     * @param days
     * @return new Date object with increased or reduced count of days by @days value
     */
    public static Date getChangedDate(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return new Date(calendar.getTimeInMillis());
    }

    /**
     * Creates new Date object from @date and set hours, minutes, seconds and milliseconds to 0
     *
     * @param date (not null)
     * @return new timeless Date object created from @date
     */
    public static Date getTimelessDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return new Date(c.getTimeInMillis());
    }

}

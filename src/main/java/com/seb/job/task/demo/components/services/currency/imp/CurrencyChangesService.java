package com.seb.job.task.demo.components.services.currency.imp;

import com.seb.job.task.demo.components.entities.currency.CurrencyRateEntity;
import com.seb.job.task.demo.components.repository.currency.ICurrencyRateRepository;
import com.seb.job.task.demo.components.services.currency.ICurrencyChangesService;
import com.seb.job.task.demo.models.currency.CurrencyChanges;
import com.seb.job.task.demo.models.currency.CurrencyChangesResponse;
import com.seb.job.task.demo.models.errors.ResponseError;
import com.seb.job.task.demo.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class CurrencyChangesService implements ICurrencyChangesService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ICurrencyRateRepository currencyChangesRepository;

    private Logger log = LoggerFactory.getLogger(CurrencyChangesService.class);
    private final String url = "http://www.lb.lt/webservices/ExchangeRates/ExchangeRates.asmx/getExchangeRatesByDate?Date=%s";
    private final String userAgent = "'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.44'";


    /**
     * Retrieves lists of CurrencyRateEntity from database if exits if not exits then from http://www.lb.lt web server.<br/>
     * Retrieved results coalesce to list of CurrencyChanges by CurrencyRateEntity.currency
     *
     * @param dateBefore
     * @param selectedDate
     * @return ordered by currency ratio increase (highest increase first) list of CurrencyChanges or empty list
     */
	@Override
	public List<CurrencyChanges> getCurrencyChangesByDates(final String dateBefore, final String selectedDate) {
		CompletableFuture<List<CurrencyRateEntity>> ratesBeforeFut = CompletableFuture.supplyAsync(() -> getCurrencyRateByDate(dateBefore));
		CompletableFuture<List<CurrencyRateEntity>> selectedRatesFut = CompletableFuture.supplyAsync(() -> getCurrencyRateByDate(selectedDate));

		CompletableFuture<List<CurrencyChanges>> result = ratesBeforeFut.thenCombine(selectedRatesFut, (ratesBefore, selectedRates) -> getCurrencyChanges(ratesBefore, selectedRates));
		try {
			return result.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		return Collections.emptyList();
	}

    /**
     * First tries to get records from cache, if records are not stored in cache then invokes other methods witch ones tries to get records from database or web server.<br/>
     * If CurrencyChangesResponse.currencyChangesList is not empty stores result to cache.
     *
     * @param date
     * @return new CurrencyChangesResponse object
     */
    @Override
    @Cacheable(value = "currencyChanges", unless = "#result.currencyChangesList.isEmpty()")
    public CurrencyChangesResponse getCurrencyChangesByDate(String date) {
        StringBuilder error = new StringBuilder();
        if (!isDateValid(date, error)) {
            log.debug("[ CURRENCY CHANGES SERVICE ] date is not valid ", error.toString());
            return emptyResponse(error.toString());
        }
        Optional<Date> oDate = DateUtil.getChangedDate(date, -1);
        if (!oDate.isPresent()) {//should never happen but check anyway
            log.debug("[ CURRENCY CHANGES SERVICE ] failed to get date before ");
            return emptyResponse("incorrect date");
        }

        List<CurrencyChanges> changes = getCurrencyChangesByDates(DateUtil.SHORT_DATE.format(oDate.get()), date);
        if (changes.isEmpty()) {
            log.debug("[ CURRENCY CHANGES SERVICE ] changes is empty");
            return emptyResponse("failed to receive changes, try again later");
        }
        return new CurrencyChangesResponse(changes, new ResponseError(""));
    }

    /**
     * First tries to get records from database by @date param.<br/>
     * If records are not stored in database then tries to get from http://www.lb.lt web server.<br/>
     * When records are fetched from web service at the end of this method all records are stored in database.
     *
     * @param date (not null)
     * @return list of CurrencyRateEntity or empty list
     */
    @Override
    public List<CurrencyRateEntity> getCurrencyRateByDate(String date) {
        List<CurrencyRateEntity> fetched = findByDate(date);
        if (!fetched.isEmpty()) {
            log.debug("[ CURRENCY CHANGES SERVICE ] data received from database");
            return fetched;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", userAgent);
        headers.setAccept(Arrays.asList(MediaType.TEXT_XML));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<List<CurrencyRateEntity>> result = null;
        try {
            result = restTemplate.exchange(String.format(url, date), HttpMethod.GET, entity, new ParameterizedTypeReference<List<CurrencyRateEntity>>() {
            });
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        if (result == null || result.getBody().isEmpty()) {
            log.debug("[ CURRENCY CHANGES SERVICE ] failed receive data from web server");
            return Collections.emptyList();
        }
        currencyChangesRepository.saveAll(result.getBody());
        return result.getBody();
    }

    /**
     * Coalesce list(@c1, @c2) objects by currency and create CurrencyChanges objects.
     *
     * @param c1 (not null)
     * @param c2 (not null)
     * @return ordered by currency ratio increase (highest increase first) list of CurrencyChanges or empty list
     */
    @Override
    public List<CurrencyChanges> getCurrencyChanges(List<CurrencyRateEntity> c1, List<CurrencyRateEntity> c2) {
        if (c1.isEmpty() || c2.isEmpty())
            return Collections.emptyList();
        List<CurrencyChanges> changes = new ArrayList<>();
        for (CurrencyRateEntity cr : c1) {
            CurrencyRateEntity cr2 = c2.stream().filter(c -> doMatch(cr, c)).findAny().orElse(null);
            if (cr2 != null) {
                changes.add(new CurrencyChanges(cr, cr2));
            }
        }
        return changes.stream().sorted(Comparator.comparing(CurrencyChanges::getRatioDiff).reversed()).collect(Collectors.toList());
    }

    /**
     * Safely checks do currency match.
     *
     * @param cr1
     * @param cr2
     * @return true if @cr1 and @cr2 currency is equal
     */
    private boolean doMatch(CurrencyRateEntity cr1, CurrencyRateEntity cr2) {
        if (cr1.getId() != null && cr1.getId().getCurrency() != null && cr2.getId() != null) {
            return cr1.getId().getCurrency().equals(cr2.getId().getCurrency());
        }
        return false;
    }

    /**
     * Retrieves list of CurrencyRateEntity from database.
     *
     * @param date (not null)
     * @return list of CurrencyRateEntity or empty list if no records found
     */
    @Override
    public List<CurrencyRateEntity> findByDate(String date) {
        return currencyChangesRepository.findByDate(date);
    }

    /**
     * Valid @date format yyyy.MM.dd.<br/>
     * If @date is not valid adds error message to @error.
     *
     * @param date
     * @param error (not null)
     * @return true if @date is valid
     */
    private boolean isDateValid(String date, StringBuilder error) {

        Optional<Date> oDate = DateUtil.getDateOf(date);
        if (!oDate.isPresent()) {
            error.append("incorrect date format");
            return false;
        }
        Date selected = DateUtil.getTimelessDate(oDate.get());
        Date lastValid = DateUtil.getTimelessDate(getLastValidDate());
        if (selected.after(lastValid)) {
            error.append(String.format("last valid date is %s", DateUtil.SHORT_DATE.format(lastValid)));
            return false;
        }
        Date firstValid = DateUtil.getTimelessDate(getFirstValidDate());
        if (selected.before(firstValid)) {
            error.append(String.format("first valid date is %s", DateUtil.SHORT_DATE.format(firstValid)));
            return false;
        }
        return true;
    }

    /**
     * @return Date object of 2014.12 and last day of moth
     */
    private Date getLastValidDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2014);
        c.set(Calendar.MONTH, Calendar.DECEMBER);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    /**
     * @return Date object of 1994.01.02
     */
    private Date getFirstValidDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 1994);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DAY_OF_MONTH, 2);//include one day for calculating one day before selected
        return c.getTime();
    }

    /**
     * @param error
     * @return new CurrencyChangesResponse with empty list and new ResponseError(@error)
     */
    private CurrencyChangesResponse emptyResponse(String error) {
        return new CurrencyChangesResponse(Collections.emptyList(), new ResponseError(error));
    }

}
